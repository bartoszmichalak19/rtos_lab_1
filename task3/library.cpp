#include "library.h"

void Library::fill_first_empty_page(std::string page)
{
    for (auto &b : book)
    {
        if (b.empty())
        {
            b = page;
            return;
        }
    }
}

void Library::read_book()
{
    if (book_status_ == BookStatus::FREE)
    {
        if (book[0].empty())
        {
            std::cout << "You cannot read empty book" << std::endl;
            return;
        }
        else if (book_status_ == BookStatus::WRITING)
        {
            std::cout << "Someone is currently writing to book" << std::endl;
            return;
        }

        const std::lock_guard<std::mutex> lock(book_guard);
        book_status_ = BookStatus::READING;
        for (auto &b : book)
        {
            b.clear();
        }
        std::cout << "threadID#" << std::this_thread::get_id() << " read book" << std::endl;
        book_status_ = BookStatus::FREE;
    }
}

void Library::write_page(const std::string page)
{
    if (book_status_ == BookStatus::FREE)
    {
        if (not book[book.size()].empty())
        {
            std::cout << "Book is full" << std::endl;
            return;
        }
        else if (book_status_ == BookStatus::READING)
        {
            std::cout << "Someone is currently reading a book" << std::endl;
            return;
        }
        const std::lock_guard<std::mutex> lock(book_guard);
        book_status_ = BookStatus::WRITING;
        fill_first_empty_page(page);
        std::cout << "threadID#" << std::this_thread::get_id() << " written one page" << std::endl;
        book_status_ = BookStatus::FREE;
    }

}