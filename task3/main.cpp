#include "library.h"
#include "threads_experiment.h"
#include <functional>

Library lib;

void read_on_threads(){
    lib.read_book();
    unsigned int delay = std::rand()%300;
    std::this_thread::sleep_for(std::chrono::milliseconds(delay));   
}

void write_on_threads(const std::string s){
        lib.write_page(s);
    unsigned int delay = std::rand()%5;
    std::this_thread::sleep_for(std::chrono::milliseconds(delay));
}

int main()
{
    std::function<void()> exp_fun = [](){
        write_on_threads("wonsz");
        read_on_threads();
        };

    std::cout << std::endl << "Task 3" << std::endl;
    auto experiment_one = new ThreadsExperiment(exp_fun, 100);
    experiment_one->start();
    delete experiment_one;
}
