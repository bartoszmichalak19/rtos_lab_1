#include <array>
#include <string>
#include <iostream>
#include <mutex>
#include <thread>

class Library
{
private:
    std::array<std::string, 20> book;
    std::mutex book_guard;

    enum class BookStatus
    {
        FREE,
        READING,
        WRITING
        
    } book_status_;

    void fill_first_empty_page(std::string page);
public:
    void read_book();
    void write_page(const std::string page);
    
};