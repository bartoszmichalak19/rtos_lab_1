# Project Title
Field of study: Advanced Applied Electronics <br >
Course: Real Time Operating Systems <br >
Laboratory list no.1.  <br >
author: Bartosz Michalak <br>

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Tasks description](#tasks)

## About <a name = "about"></a>

This project is created to implement multithread applications for RTOS
laboratory claesses. Apps are written in C++ using existing libraries. 

## Getting Started <a name = "getting_started"></a>

Short introduction just to run code related with tasks. They are divided into three directories, every one has simmilar structure. There is a simple cmake set up and few scripts to run for each task directory. 

### Prerequisites

Preffered linux OS (tested on ubuntu 20.04) and cmake (3.10+ version)


### Building and Running apps <a name = "usage"></a>

Example of usage for one of tasks.

```sh
cd task3
./build_app.sh
./run.sh
```

## Tasks descriptions <a name = "tasks"></a>

**Task1 and 2** <br > 
There are two functions, both executed on 16 threads tries to add some value to global variable "g_counter".
```cpp
void safe_add(const int to_add)
{
    const std::lock_guard<std::mutex> lock(g_c_mutex);
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
    g_counter += to_add;
    std::cout << "threadID# " << std::this_thread::get_id() << " counter value: " << g_counter << std::endl;
}
```
```cpp
void unsafe_add(const int to_add)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
    g_counter += to_add;
    std::cout << "threadID# " << std::this_thread::get_id() << " counter value: " << g_counter << std::endl;
}
```
After run on standard output you will observe that function without mutex (task1) does some insane operations, few threads tires to change variable at the same time. 
Function  safe_add() does it better, because it locks other therads with mutex. 
Class ThreadsExperiment is responsible for preparing environment for testing app. There are two functions working on 16 threads 

**Task 3** 

I used library as a abstract example of reader and writer of one book.
```cpp
private:
    std::array<std::string, 20> book;
    std::mutex book_guard;

    enum class BookStatus
    {
        FREE,
        READING,
        WRITING
        
    } book_status_;
```
Mutex is used to hold the book, it has also few states that help to keep rules from
the description. In main, I showed an example of usage. One each thread writer writes to book each (randomvalue < 5 milisecond)  and read book every (randomvalue < 300 milisecond). The standard output shows that class "Library" keep the book as it should. 

**Task 4** 

I used class PiEstimator that in constructor takes number of samples in monte carlo method. This class has function calculate which returns estimated pi value. It is called inside x numbers of threads. At the begginig I start main thread that waits for condition variable.
```cpp
void main_thread_fun()
{
    std::unique_lock<std::mutex> lck(mutex);
    cond_var.wait(lck, [] { return whole_threads_sampled; });
    pi_mean /= number_of_sampling_threads;
    std::cout << "Mean pi value: " << pi_mean << std::endl;
}
```
 Sum of pi values is stored in global variable "pi_mean", when whole threads compute pi value, the last thread releases condition variable and calculate mean pi value of whole threads. Then this value is printed out on standard output.
 ```cpp
void pi_estimator_thread_fun()
{
    {
        PiEstimator monte_carlo_pi(number_of_samples);
        std::lock_guard<std::mutex> lck(mutex);
        pi_mean += monte_carlo_pi.calculate();
        counter++;
        if (counter >= number_of_sampling_threads)
        {
            whole_threads_sampled = true;
            cond_var.notify_one();
        }
    }
}
```



