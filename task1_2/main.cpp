#include <thread>
#include <iostream>
#include <mutex>
#include <chrono>
#include <functional>
#include "threads_experiment.h"

int g_counter = 0;
std::mutex g_c_mutex;

void safe_add(const int to_add)
{
    const std::lock_guard<std::mutex> lock(g_c_mutex);
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
    g_counter += to_add;
    std::cout << "threadID# " << std::this_thread::get_id() << " counter value: " << g_counter << std::endl;
}

void unsafe_add(const int to_add)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(300));
    g_counter += to_add;
    std::cout << "threadID# " << std::this_thread::get_id() << " counter value: " << g_counter << std::endl;
}

int main()
{
    const int plus_value = 5;
    std::function<void()> exp_fun = [](){unsafe_add(plus_value);};

    std::cout << std::endl << "Task 1 without mutex" << std::endl;
    auto experiment_one = new ThreadsExperiment(exp_fun, 16);
    experiment_one->start();
    delete experiment_one;

    g_counter = 0;

    std::cout << std::endl << "Task 2 with mutex" << std::endl;
    exp_fun = [](){safe_add(plus_value);};
    auto experiment_two = new ThreadsExperiment(exp_fun,16);
    experiment_two->start();
    delete experiment_two;
}
