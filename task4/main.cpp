#include "pi_estimator.h"
#include "threads_experiment.h"
#include <functional>
#include <iostream>
#include <condition_variable>
#include <thread>

std::mutex mutex;
std::condition_variable cond_var;
const auto number_of_samples = 100000U;
const auto number_of_sampling_threads = 100;
auto counter = 0U;
float pi_mean = 0.0;
bool whole_threads_sampled = false;

void main_thread_fun()
{
    std::unique_lock<std::mutex> lck(mutex);
    cond_var.wait(lck, [] { return whole_threads_sampled; });
    pi_mean /= number_of_sampling_threads;
    std::cout << "Mean pi value: " << pi_mean << std::endl;
}
void pi_estimator_thread_fun()
{
    {
        PiEstimator monte_carlo_pi(number_of_samples);
        std::lock_guard<std::mutex> lck(mutex);
        pi_mean += monte_carlo_pi.calculate();
        counter++;
        if (counter >= number_of_sampling_threads)
        {
            whole_threads_sampled = true;
            cond_var.notify_one();
        }
    }
}

int main()
{   
    std::function<void()> exp_fun = [](){
        main_thread_fun();
        };
    ThreadsExperiment experiment_one_main_thread(exp_fun, 1);

    exp_fun = [](){
        pi_estimator_thread_fun();
        };
    ThreadsExperiment experiment_one(exp_fun, number_of_sampling_threads);
    
    experiment_one_main_thread.start();
    experiment_one.start();
}
